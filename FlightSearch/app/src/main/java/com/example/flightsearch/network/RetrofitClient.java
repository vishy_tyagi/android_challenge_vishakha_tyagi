package com.example.flightsearch.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static Retrofit mRetrofitClient;
    private static final String BASE_URL = "https://www.mocky.io/v2/";

   private static Gson gson = new GsonBuilder()
           .setLenient()
            .create();

    public static Retrofit getInstance(){
        if (mRetrofitClient == null) {
            mRetrofitClient = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(UnsafeHttpClient.getUnsafeOkHttpClient())
                    .build();
        }
        return mRetrofitClient;

    }
}
