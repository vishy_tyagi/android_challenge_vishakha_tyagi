package com.example.flightsearch.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

public class Appendix {

    @SerializedName("airlines")
    @Expose
    private HashMap<String, String> airlines;
    @SerializedName("airports")
    @Expose
    private HashMap<String, String> airports;
    @SerializedName("providers")
    @Expose
    private HashMap<String, String> providers;

    public HashMap<String, String> getAirlines() {
        return airlines;
    }

    public void setAirlines(HashMap<String, String> airlines) {
        this.airlines = airlines;
    }

    public HashMap<String, String> getAirports() {
        return airports;
    }

    public void setAirports(HashMap<String, String> airports) {
        this.airports = airports;
    }

    public HashMap<String, String> getProviders() {
        return providers;
    }

    public void setProviders(HashMap<String, String> providers) {
        this.providers = providers;
    }

}