package com.example.flightsearch;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.flightsearch.fragments.FlightSearchFragment;
import com.example.flightsearch.utils.Util;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, FlightSearchFragment.getInstance()).commit();
    }
}
