package com.example.flightsearch.binders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.flightsearch.ListAdapter;
import com.example.flightsearch.R;
import com.example.flightsearch.databinding.FareRowBinding;
import com.example.flightsearch.pojo.Fare;
import com.example.flightsearch.pojo.Flight;
import com.example.flightsearch.pojo.SearchItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class BindingAdapter {

    @androidx.databinding.BindingAdapter("load")
    public static void loadList(RecyclerView recyclerView, SearchItem searchItem){
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(recyclerView.getContext());
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(linearLayoutManager);
        ListAdapter flightsAdapter = new ListAdapter(searchItem);
        recyclerView.setAdapter(flightsAdapter);
    }

    @androidx.databinding.BindingAdapter({"entries", "mapping"})
    public static void setEntries(ViewGroup linearLayout, List<Fare> fares, HashMap<String, String> map){
        Log.d("TAG", linearLayout.getChildCount() + "");
        Collections.sort(fares);
        linearLayout.removeAllViews();
        for (Fare fare : fares){
            FareRowBinding binding = DataBindingUtil.inflate(LayoutInflater.from(linearLayout.getContext()), R.layout.fare_row, linearLayout, false);
            binding.setMFare(fare);
            binding.setProviders(map);
            linearLayout.addView(binding.getRoot());
        }
    }
}
