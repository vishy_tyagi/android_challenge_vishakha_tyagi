package com.example.flightsearch.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class Util {


    public static ArrayList<String> getStaticAirports() {
        ArrayList<String> list = new ArrayList<>();
        list.add("Delhi");
        list.add("Mumbai");
        return list;
    }

    public static String getTime(long millis) {

        DateFormat formatter = new SimpleDateFormat("HH:mm", Locale.US);
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        return formatter.format(new Date(millis));
    }
}
