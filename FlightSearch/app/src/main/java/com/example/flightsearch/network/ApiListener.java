package com.example.flightsearch.network;

public interface ApiListener<T extends Object> {

    public void onResponse(T response);

    public void onError(Throwable t);
}
