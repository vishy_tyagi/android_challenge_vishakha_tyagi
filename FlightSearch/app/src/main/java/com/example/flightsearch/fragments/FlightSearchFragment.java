package com.example.flightsearch.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.example.flightsearch.R;
import com.example.flightsearch.databinding.FlightSearchLayoutBinding;
import com.example.flightsearch.listeners.OnclickHandler;

public class FlightSearchFragment extends Fragment implements OnclickHandler {

    private FlightSearchLayoutBinding binding;

    public static FlightSearchFragment getInstance(){
        FlightSearchFragment flightSearchFragment = new FlightSearchFragment();
        return flightSearchFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle bundle){
        binding = DataBindingUtil.inflate(inflater, R.layout.flight_search_layout, parent, false);
        binding.setHandler(this); // To handle user actions
        return binding.getRoot();
    }

    //handling search click
    //For now it will show Delhi to Mumbai Flights irrespective of user input as given in Sample Api
    @Override
    public void onClick() {
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, FlightListFragment.getInstance()).addToBackStack(null).commit();
    }
}
