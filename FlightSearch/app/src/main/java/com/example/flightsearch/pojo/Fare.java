package com.example.flightsearch.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Fare implements Comparable<Fare> {

    @SerializedName("providerId")
    @Expose
    private Integer providerId;
    @SerializedName("fare")
    @Expose
    private Integer fare;

    public Integer getProviderId() {
        return providerId;
    }

    public void setProviderId(Integer providerId) {
        this.providerId = providerId;
    }

    public Integer getFare() {
        return fare;
    }

    public void setFare(Integer fare) {
        this.fare = fare;
    }

    @Override
    public int compareTo(Fare fare) {
        return this.fare - fare.fare;
    }
}