package com.example.flightsearch.utils;

import com.example.flightsearch.pojo.Flight;

import java.util.Comparator;

public class FlightTimeComparator implements Comparator<Flight> {

    private boolean islanding; // To check if sort by take off or landing times

    public FlightTimeComparator(boolean islanding){
        this.islanding = islanding;
    }

    @Override
    public int compare(Flight flight1, Flight flight2) {
        if(islanding) {
            return Long.compare(flight1.getArrivalTime(), flight2.getArrivalTime());
        }
        else {
            return Long.compare(flight1.getDepartureTime(), flight2.getDepartureTime());
        }
    }
}
