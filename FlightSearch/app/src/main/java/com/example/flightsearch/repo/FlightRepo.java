package com.example.flightsearch.repo;

import com.example.flightsearch.network.ApiInterface;
import com.example.flightsearch.network.ApiListener;
import com.example.flightsearch.network.RetrofitClient;
import com.example.flightsearch.pojo.SearchItem;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

// This repository might fetch data from network or from Database according to requirement
// Repository behaves as single source of data
public class FlightRepo {

    private static FlightRepo flightRepo;

    public static FlightRepo getInstance(){
        if (flightRepo == null){
             flightRepo = new FlightRepo();
        }
        return flightRepo;
    }

    public void fetchFlightsInfo(final ApiListener<SearchItem> apiListener){
        RetrofitClient.getInstance().create(ApiInterface.class).getSearchItem().enqueue(new Callback<SearchItem>() {

            @Override
            public void onResponse(Call<SearchItem> call, Response<SearchItem> response) {

                apiListener.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<SearchItem> call, Throwable t) {

                apiListener.onError(t);
            }
        });
    }
}
