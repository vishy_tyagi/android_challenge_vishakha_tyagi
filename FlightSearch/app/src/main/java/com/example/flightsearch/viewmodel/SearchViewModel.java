package com.example.flightsearch.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.flightsearch.network.ApiListener;
import com.example.flightsearch.pojo.SearchItem;
import com.example.flightsearch.repo.FlightRepo;
import com.example.flightsearch.utils.FareComparator;
import com.example.flightsearch.utils.FlightTimeComparator;

import java.util.Collections;

// Fetch required data from a data repository
// observes user actions and keep updating UI on data change
// handle sorting fliters
public class SearchViewModel extends ViewModel {


    private FlightRepo mFlightRepo = FlightRepo.getInstance();
    private MutableLiveData<SearchItem> searchItemMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<SearchItem> getSearchItemMutableLiveData(){
        return searchItemMutableLiveData;
    }

    public void fetchSearchApi(){
        mFlightRepo.fetchFlightsInfo(new ApiListener<SearchItem>(){

            @Override
            public void onResponse(SearchItem response) {
                searchItemMutableLiveData.setValue(response);
            }

            @Override
            public void onError(Throwable t) {
                searchItemMutableLiveData.setValue(null);
            }
        });
    }

    public void sortByMinimumFare(){
        SearchItem searchItem = searchItemMutableLiveData.getValue();
        if(searchItem != null && searchItem.getFlights() != null) {
            Collections.sort(searchItem.getFlights(), new FareComparator());
            searchItemMutableLiveData.setValue(searchItem);
        }

    }

    public void sortByTakeOff(){
        SearchItem searchItem = searchItemMutableLiveData.getValue();
        if(searchItem != null && searchItem.getFlights() != null) {
            Collections.sort(searchItem.getFlights(), new FlightTimeComparator(false));
            searchItemMutableLiveData.setValue(searchItem);
        }
    }

    public void sortByLanding(){
        SearchItem searchItem = searchItemMutableLiveData.getValue();
        if(searchItem != null && searchItem.getFlights() != null) {
            Collections.sort(searchItem.getFlights(), new FlightTimeComparator(true));
            searchItemMutableLiveData.setValue(searchItem);
        }
    }

    public void clearFilter() {
        fetchSearchApi();
    }
}
