package com.example.flightsearch.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.flightsearch.R;
import com.example.flightsearch.databinding.FlightListLayoutBinding;
import com.example.flightsearch.pojo.SearchItem;
import com.example.flightsearch.utils.Constants;
import com.example.flightsearch.viewmodel.SearchViewModel;

public class FlightListFragment extends Fragment  {

    FlightListLayoutBinding binding;
    private SearchViewModel searchViewModel;

    public static FlightListFragment getInstance() {
        FlightListFragment flightListFragment = new FlightListFragment();
        return flightListFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle bundle){
        binding = DataBindingUtil.inflate(inflater, R.layout.flight_list_layout, parent, false);
        binding.setIsProgress(true);
        return binding.getRoot();
    }

    // In this method we fetches data with SearchViewModel and observes it with LiveData
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        searchViewModel = ViewModelProviders.of(this).get(SearchViewModel.class);
        searchViewModel.fetchSearchApi();
        searchViewModel.getSearchItemMutableLiveData().observe(this, new Observer<SearchItem>() {
            @Override
            public void onChanged(SearchItem searchItem) {

                binding.setIsProgress(false);
                if (searchItem == null){
                    //  handling error view
                    binding.setMError(true);
                }
                else {
                    binding.setMSearchItem(searchItem);
                }
                binding.executePendingBindings();
            }
        });
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.add(Constants.SORT_BY_FARE);
        menu.add(Constants.SORT_BY_TAKE_OFF);
        menu.add(Constants.SORT_BY_LANDING);
        menu.add(Constants.CLEAR_FILTER);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getTitle().toString()) {

            case Constants.SORT_BY_FARE:
                searchViewModel.sortByMinimumFare();
                return true;

            case Constants.SORT_BY_TAKE_OFF:
                searchViewModel.sortByTakeOff();
                return true;

            case Constants.SORT_BY_LANDING:
                searchViewModel.sortByLanding();
                return true;
            case Constants.CLEAR_FILTER:
                searchViewModel.clearFilter();
            default:
                break;
        }

        return false;
    }

}
