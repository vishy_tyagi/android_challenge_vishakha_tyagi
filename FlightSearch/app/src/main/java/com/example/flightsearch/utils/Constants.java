package com.example.flightsearch.utils;

public interface Constants {

    String SORT_BY_FARE = "SortByFare";
    String SORT_BY_TAKE_OFF = "sortByTakeOff";
    String SORT_BY_LANDING = "sortByLanding";
    String CLEAR_FILTER = "ClearAll";
}
