package com.example.flightsearch;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.flightsearch.databinding.FlightRowItemBinding;
import com.example.flightsearch.pojo.Appendix;
import com.example.flightsearch.pojo.Flight;
import com.example.flightsearch.pojo.SearchItem;

import java.util.ArrayList;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.FlightViewHolder> {

    private ArrayList<Flight> flights;
    private SearchItem searchItem;
    private  Appendix appendix;


    public ListAdapter(SearchItem data){
        this.searchItem = data;
        if (searchItem != null && searchItem.getFlights() != null){
            flights = searchItem.getFlights();
        }
        if(searchItem != null && searchItem.getAppendix() != null ){
            appendix = searchItem.getAppendix();
        }
    }

    @NonNull
    @Override
    public FlightViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        FlightRowItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.flight_row_item, parent, false);
        return new FlightViewHolder(binding, appendix);
    }

    @Override
    public void onBindViewHolder(@NonNull FlightViewHolder holder, int position) {
        holder.bind(flights.get(position));

    }

    @Override
    public int getItemCount() {
        return flights.size();
    }

    public static class FlightViewHolder extends RecyclerView.ViewHolder{

        public FlightRowItemBinding flightRowItemBinding;
        private Appendix appendix;

        public FlightViewHolder(FlightRowItemBinding itemView, Appendix appendix) {
            super(itemView.getRoot());
            this.appendix = appendix;
            this.flightRowItemBinding = itemView;
        }

        public void bind(Flight flight) {
            flightRowItemBinding.setMFlight(flight);
            flightRowItemBinding.setMAppendix(appendix);
            flightRowItemBinding.executePendingBindings();
        }
    }
}
