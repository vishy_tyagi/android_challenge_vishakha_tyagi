package com.example.flightsearch.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchItem implements Serializable {

    @SerializedName("appendix")
    @Expose
    private Appendix appendix;
    @SerializedName("flights")
    @Expose
    private ArrayList<Flight> flights = null;

    public Appendix getAppendix() {
        return appendix;
    }

    public void setAppendix(Appendix appendix) {
        this.appendix = appendix;
    }

    public ArrayList<Flight> getFlights() {
        return flights;
    }

    public void setFlights(ArrayList<Flight> flights) {
        this.flights = flights;
    }

}