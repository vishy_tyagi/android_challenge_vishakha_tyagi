package com.example.flightsearch.utils;

import com.example.flightsearch.pojo.Fare;
import com.example.flightsearch.pojo.Flight;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FareComparator implements Comparator<Flight> {

    @Override
    public int compare(Flight flight1, Flight flight2) {
        return getCheapest(flight1).getFare() - getCheapest(flight2).getFare();
    }


    public Fare getCheapest(Flight flight){
        List<Fare> fares = flight.getFares();
        Collections.sort(fares);
        return fares.get(0);
    }
}
