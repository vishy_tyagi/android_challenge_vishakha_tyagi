package com.example.flightsearch.network;

import com.example.flightsearch.pojo.SearchItem;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface ApiInterface {

    @Headers("Content-Type: application/json")
    @GET("5979c6731100001e039edcb3/")
    Call<SearchItem> getSearchItem();
}
